class UserList extends Base {
  constructor() {
    super();
    this.collection = "people/";
    this.url = this.apiUrl + this.collection;
    // this.categoriesUrl = "http://localhost:3000/api/categories/";
    this.init();
    this.sort = 'SORT_BY_NAME';
    this.categoryData = [];
  }

  init() {
    var that = this;
    this.callback = function (err, data) {
      if (err !== null) {
        console.log(err);
      } else {
        console.log(data);

        if (that.sort === 'SORT_BY_NAME') {
          data = data.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
        } else if (that.sort === 'SORT_BY_ID') {
          data = data.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
        }

        /**
         * creation data time from mongodb id (only for test)
         * */
        console.log("sort by name ", data);
        let timestamp;
        for (let i in data) {
          timestamp = data[i].id.toString().substring(0, 8)
          console.log(new Date(parseInt(timestamp, 16) * 1000));
        }

        //  console.log(that.dynamicSort(data));
        var str = "";
        for (var i = 0; i < data.length; i++) {
          str += `Name: ${data[i].name} <br/> Description: ${data[i].description} <br/>
                        <button class="btn-warning btn js-scroll-trigger" href="javascript:void(0)" onclick="createUserList.showHideDiv(${i})">update </button> |

                        <button class="btn-danger btn js-scroll-trigger" href="javascript:void(0)" onclick="createUserList.deleteItem('${data[i].id}')">delete </button>
<div id="update_${i}" style="display: none"><br />
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align: right">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <input type="text" id="newUserName_${data[i].id}" value="${data[i].name}" placeholder="name"/>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <input type="text" id="newUserPseudo_${data[i].id}" value="${data[i].pseudo}" placeholder="pseudo"/>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <input type="text" id="newUserDescription_${data[i].id}" value="${data[i].description}" placeholder="description"/>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align: left">
        <select multiple="multiple" id="newCategorySelect"></select>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button style="margin: 5px;" class="btn-success btn js-scroll-trigger" onclick="createUserList.updateItem('${data[i].id}')">Send</button>
      </div>
    </div>
    </div>
</div><br/><br/>`
        }
        document.getElementById("listUsers").innerHTML = str;
      }
    }
    this.getCategoriesJSON();
    this.getJSON();
  }

  createFormSubmit() {
    var that = this;
    var dataValues = {
      name: document.getElementById("userName").value,
      pseudo: document.getElementById("userPseudo").value,
      description: document.getElementById("userDescription").value,
      categories: $('#categorySelect').val()
    }

    if (dataValues.name && dataValues.pseudo && dataValues.categories.length && dataValues.description) {
      $.ajax({
        url: this.url,
        type: 'POST',
        data: dataValues,
        success: function (response) {
          console.log(response);
        }
      }).done(function () {
        // do some things for update cthe category list
        console.log("done");
        that.getJSON();
      }).fail(function () {
        console.log("error");
      });
    } else {
      alert("All the fields is required!");
    }
  }

  updateItem(id) {
    var that = this;
    $.ajax({
      url: this.url + id,
      type: 'PUT',
      data: {
        name: document.getElementById("newUserName_" + id).value,
        pseudo: document.getElementById("newUserPseudo_" + id).value,
        description: document.getElementById("newUserDescription_" + id).value,
        categories: $('#newCategorySelect').val()
      },
      success: function (response) {
        console.log(response);
      },
    }).done(function () {
      console.log("done");
      that.getJSON()
    }).fail(function () {
      console.log("error getJSON");
    });
  }

  createCategoriesSelect(categoriesArray) {
    console.log('document.getElementById( "categorySelect")');
    console.log(document.getElementById("categorySelect"));
    var strA = '';
    for (var c in categoriesArray) {
      console.log(categoriesArray[c].name)
      strA += '<option value="' + categoriesArray[c].name + '">' + categoriesArray[c].name + '</option>';
    }
    strA += '</select>';
    //$("#CategorySelect").html(strA);
    $.each($('select'), function (index, value) {
      $("select").html(strA);
      //console.log( index + ": " + value );
    });
  }

  getCategoriesJSON() {
    var that = this;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', this.apiUrl + "categories/", true);
    xhr.responseType = 'json';
    xhr.onload = function () {
      var status = xhr.status;
      //this.data = xhr.response;
      if (status === 200) {
        that.categoryData = xhr.response;
        console.log("###############UserList class - get categories from categoryList###################");
        console.log(that.categoryData);
        console.log("###############/UserList class - get categories from categoryList###################");
        that.createCategoriesSelect(that.categoryData)
      } else {
        console.log("Can't receive the categories data");
      }
    };
    xhr.send();
  }
}
