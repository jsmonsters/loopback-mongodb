class Base {
  constructor() {
    this.sort = 'SORT_BY_NAME';
    this.apiUrl =  "http://localhost:3000/api/";
    this.showMainNav();
  }

  showMainNav() {
    $("#nav").html('<a href="users.html">Users</a> | <a href="categories.html">Categories</a>');
  }

  deleteItem(id) {
    var that = this;
    console.log("deleteItem id: " + id);
    $.ajax({
      url: this.url + id,
      type: 'DELETE',
      success: function (response) {
        console.log(response);
      }
    }).done(function () {
      console.log("done");
      that.getJSON(that.url, that.callback)
    }).fail(function () {
      console.log("error");
    });
  }

  showHideDiv(id) {
    var x = document.getElementById("update_" + id);
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  getJSON() {
    var that = this;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', this.url, true);
    xhr.responseType = 'json';
    xhr.onload = function () {
      var status = xhr.status;
      //this.data = xhr.response;
      if (status === 200) {
        that.callback(null, xhr.response, that);
      } else {
        that.callback(status, xhr.response, that);
      }
    };
    xhr.send();
  };

  updateSortCondition(sortCondition){
    this.sort = sortCondition;
    this.getJSON();
  }
}
