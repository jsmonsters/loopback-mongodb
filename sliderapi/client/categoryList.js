class CategoryList extends Base {
  constructor() {
    super();
    this.sort = 'SORT_BY_NAME';
    this.collection = "categories/";
    this.url = this.apiUrl + this.collection;
    this.init();
  }

  init() {
    var that = this;
    this.callback = function (err, data) {
      if (err !== null) {
        console.log(err);
      } else {
        console.log(data);
        that.data = data;
        if(that.sort === 'SORT_BY_NAME'){
          data = data.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
        } else if(that.sort === 'SORT_BY_ID'){
          data = data.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
        }

        /**
         * creation data time from mongodb id (only for test)
         * */
        console.log("sort by name ", data);
        let timestamp;
        for(let i in data){
           timestamp = data[i].id.toString().substring(0,8)
           console.log(new Date( parseInt( timestamp, 16 ) * 1000 ));
        }

      //  console.log(that.dynamicSort(data));
        var str = "";
        for (var i = 0; i < data.length; i++) {
          str += `Name: ${data[i].name} <br/> Description: ${data[i].description} <br/>
                        <button class="btn-warning btn js-scroll-trigger" href="javascript:void(0)" onclick="createCategoryList.showHideDiv(${i})">update </button> |

                        <button class="btn-danger btn js-scroll-trigger" href="javascript:void(0)" onclick="createCategoryList.deleteItem('${data[i].id}')">delete </button>
<div id="update_${i}" style="display: none"><br />
    <div>
        <input type="text" name="name" id="newName_${data[i].id}" value="${data[i].name}" placeholder="name"/>
        <input type="text" name="description" id="newDescription_${data[i].id}" value="${data[i].description}" placeholder="description"/>
        <button style="margin: 5px;" class="btn-success btn js-scroll-trigger" onclick="createCategoryList.updateItem('${data[i].id}')">Send</button>
    </div>
</div><br/><br/>`
        }
        document.getElementById("listCategories").innerHTML = str;
      }
    }
    this.getJSON();
  }

  createFormSubmit() {
    var that = this;
    var dataValues = {
      name: document.getElementById("categoryName").value,
      description: document.getElementById("categoryDescription").value
    }
    if (dataValues.name && dataValues.description) {
      $.ajax({
        url: this.url,
        type: 'POST',
        data: dataValues,
        success: function (response) {
          console.log(response);
        }
      }).done(function () {
        // do some things for update cthe category list
        console.log("done");
        that.getJSON();
      }).fail(function () {
        console.log("error");
      });
    } else {
      alert("All the fields is required!");
    }
  }

  updateItem(id) {
    var that = this;
    $.ajax({
      url: this.url + id,
      type: 'PUT',
      data: {
        name: document.getElementById("newName_" + id).value,
        description: document.getElementById("newDescription_" + id).value
      },
      success: function (response) {
        console.log(response);
      },
    }).done(function () {
      console.log("done");
      that.getJSON()
    }).fail(function () {
      alert("error");
    });
  }
}
