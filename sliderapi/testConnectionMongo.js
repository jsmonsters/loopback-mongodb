const { MongoClient } = require('mongodb');
// or as an es module:
// import { MongoClient } from 'mongodb'

// Connection URL
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);

// Database Name
const dbName = 'js';

async function main() {
  // Use connect method to connect to the server
  await client.connect();
  console.log('Connected successfully to server');
  const db = client.db(dbName);
  const collection = db.collection('category');


  const insertResult = await collection.insertMany([{
    name: 'new Record 1',
    description: 'ha ha'
  },{
    name: 'new Record 2',
    description: 'hi hi'
  }]);
  console.log('Inserted documents =>', insertResult);

  const findResult = await collection.find({}).toArray();

  console.log("###########################category##################################");
  console.log('Found documents =>', findResult);
  console.log("###########################/category##################################");

  // the following code examples can be pasted here...

  return 'done.';
}

main()
  .then(console.log)
  .catch(console.error)
  .finally(() => client.close());
